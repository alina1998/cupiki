import 'dart:io';

import 'package:flutter/material.dart';


void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MainWindoy(),
  ));
}




class MainWindoy extends StatelessWidget{
  @override

  Widget build (BuildContext context){
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
           children: [
             Row(
               mainAxisAlignment: MainAxisAlignment.start,
               children:[

                 IconButton(
                   icon: Icon(Icons.volume_up, size: 60.0, color: Colors.purple,),
                   onPressed: () {},),




               ],

             ),
             RaisedButton(onPressed: (){
               Navigator.push(context, MaterialPageRoute(builder: (context) => WindoyOne()));
             },
               child: Text('Поле', style: TextStyle(color: Colors.purple, fontSize: 30),),
          ),
             RaisedButton(onPressed: (){
               Navigator.push(context, MaterialPageRoute(builder: (context) => WindoyTwo()));
             },
                 child: Text('Параметры', style: TextStyle(color: Colors.purple, fontSize: 30),),
             ),

             RaisedButton(onPressed: (){
               Navigator.push(context, MaterialPageRoute(builder: (context) => WindoyThree()));
             },
               child: Text('Инструкция', style: TextStyle(color: Colors.purple, fontSize: 30),),
             ),

             RaisedButton(
               child: Text('Exit', style: TextStyle(color: Colors.purple, fontSize: 30),),
               //onPressed: () => Navigator.of(context).pop(null),

               onPressed: ()=> exit(0),
             )





        ]

    )
    )
    );
  }
}


class WindoyTwo extends StatelessWidget{

  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(backgroundColor: Color(0x673AB7),),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [


              Text('Выбирите число кубиков', style: TextStyle(fontSize: 30, fontStyle: FontStyle.italic), ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.looks_one), onPressed: () {},),

                  IconButton(
                    icon: Icon(Icons.looks_two), onPressed: () {},),

                  IconButton(
                    icon: Icon(Icons.add_circle_outline), onPressed: () {},),
                ],
              ),

              Text('Выбирите цвет кубика', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic),),
              Text(''),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.white,
                        onPressed: () {},
                        child:Image.asset('images/photo_2019-07-18_19-17-01.jpg',
                          width: 70.0, height: 70.0,)),
                    RaisedButton(
                      color: Colors.white,
                        onPressed: () {},
                        child:Image.asset('images/photo_2019-07-18_19-18-00.jpg',
                          width: 70.0, height: 70.0,)),


                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Container(
                  height: 1.5,
                  color: Colors.black,
                ),
              ),


              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text('Включить темную тему', style: TextStyle(fontSize: 25.0),),
                    SwitchWidget(),

                  ],
                ),
              ),

            ],

          ),

        ),

    );
  }
}

class SwitchWidget extends StatefulWidget {
  @override
  _SwitchWidgetState createState() => _SwitchWidgetState(); //blalbalbala
}

class _SwitchWidgetState extends State<SwitchWidget> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Switch(value: isSwitched, onChanged: (value){
        setState((){
          isSwitched = value;
        });
      },
        activeColor: Colors.blue[100],
        activeTrackColor: Colors.blue,
      ),
    );
  }
}




class WindoyOne extends StatelessWidget{

  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(backgroundColor: Color(0x673AB7),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RaisedButton(
                  color: Colors.white,
                    onPressed: () {},
                    child:Image.asset('images/photo_2019-07-18_19-17-01.jpg',

                      width: 100.0, height: 100.0,)),




              ],

            ),


          ],

        ),

      ),

    );
  }
}



class WindoyThree extends StatelessWidget{

  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(backgroundColor: Color(0x673AB7),),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                  Text('Если на поле вам нужен', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic,),),
                  Text('только один кубик,', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('то можите переходить', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('по вкладке поле.', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('Если вам нужно нескоько ', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('кубиков или сощитать', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('сумму выпавших чисел,', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('переходите по вкладке', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),
                  Text('параметры', style: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic)),


            ],

          ),
        ),

      ),

    );
  }
}